#!/usr/bin/env python3.10
import os
import sys

from util import original

if __name__ == '__main__':
    with open(original, 'r') as file:
        rolling = file.readlines()
    A = 0

    for line in rolling:
        if A == 1 and '- [' in line:
            print(line, end='')
        if '### Tasks' in line and A == 0:
            A = 1
            continue
        if '## ' in line and A == 1:
            break

    sys.exit(0)
