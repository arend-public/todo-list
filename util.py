import os
import subprocess
import sys
from datetime import datetime, date

DATESTAMP_FORMAT = '%Y-%m-%d'
date_str = date.strftime(datetime.now(), DATESTAMP_FORMAT)
SRC = os.path.dirname(os.path.abspath(__file__))
original = os.path.join(SRC, 'rolling.md')
backup = os.path.join(SRC, 'rolling.md.bkp')


def execute(cmdline: list[str]) -> tuple[int, str, str]:
    proc = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            stdin=subprocess.DEVNULL, shell=False, encoding='utf-8')
    ret_code = proc.wait()
    stdout, stderr = proc.communicate()
    return ret_code, stdout, stderr


def exec_wrapper(cmdline: list[str]) -> str:
    rc, out, err = execute(cmdline)
    if rc != 0:
        print(err, file=sys.stderr)
        raise SystemExit(rc)
    if not out:
        return err
    return out


def runtime(fn) -> None:
    exec_wrapper(['/bin/cp', original, backup])

    def on_error(_err):
        try:
            print(f'{type(_err)}: {str(_err)}')
            exec_wrapper(['/bin/mv', backup, original])
        except BaseException as _error:
            print(f'{type(_error)}: {str(_error)}')
    try:
        rc = fn()
    except BaseException as error:
        on_error(error)
        rc = 1
    sys.exit(rc)
