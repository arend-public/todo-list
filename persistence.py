from util import original

TYPE_TASK = 'TASK'
TYPE_TASKS = 'TASKS'
TYPE_DATE = 'DATE'
TYPE_NONE = 'NONE'


def is_task_line(line):
    return '- [' in line


def is_tasks_line(line):
    return '### Tasks' == line.strip()


def is_date_line(line):
    return '##' in line and not is_tasks_line(line)


def get_line_type(line):
    if is_task_line(line):
        return TYPE_TASK
    if is_tasks_line(line):
        return TYPE_TASKS
    if is_date_line(line):
        return TYPE_DATE
    return TYPE_NONE


class Task:
    def __init__(self, date, completed, value):
        self.date = date
        self.completed = completed
        self.value = value

    @staticmethod
    def from_line(line, date):
        return Task(date, '[x]' in line, line.strip().split('] ').pop())

    def raw(self):
        return f'- [{"x" if self.completed else " "}] {self.value}'

    def __str__(self):
        return self.value


class TaskList:
    def __init__(self, date):
        self.date = date
        self.tasks: list[Task] = []

    def prepend_task(self, task):
        self.tasks.insert(0, task)

    def add_task(self, task):
        self.tasks.append(task)

    def tasks_as_values(self):
        return [str(x) for x in self.tasks]

    def tasks_raw(self):
        return [x.raw() for x in self.tasks]

    def incomplete_tasks(self):
        return [str(x) for x in self.tasks if not x.completed]

    def incomplete_tasks_raw(self):
        return [x.raw() for x in self.tasks if not x.completed]

    def complete_task(self, value):
        for task in self.tasks:
            if str(task) == value:
                task.completed = True
                return
        raise RuntimeError(f'Task: "{value}" not found')


class Writer:
    def __init__(self, tasklists: list[TaskList]):
        self.file = open(original, 'w')
        self.tasklists = tasklists

    def write(self, *values, sep=' ', end='\n', flush=True):
        print(*values, sep, file=self.file, flush=flush, end=end)

    def write_task(self, task: Task):
        self.write(task.raw())

    def write_tasklist(self, tasklist: TaskList):
        self.write(f'## {tasklist.date}')
        self.write(f'### Tasks')
        for task in tasklist.tasks:
            self.write_task(task)
        self.write()

    def write_out(self):
        for tasklist in self.tasklists:
            self.write_tasklist(tasklist)

    def __del__(self):
        self.file.close()


class Persistence:
    def __init__(self):
        with open(original, 'r') as file:
            self.storage = file.readlines()
        self.tasklists: list[TaskList] = []
        self.read_tasks()

    def read_tasks(self):
        index = -1
        for line in self.storage:
            line_type = get_line_type(line)
            if line_type == TYPE_NONE:
                continue
            if line_type == TYPE_TASK:
                tl = self.tasklists[index]
                task = Task.from_line(line, tl.date)
                tl.add_task(task)
                continue
            if line_type == TYPE_TASKS:
                continue
            if line_type == TYPE_DATE:
                index += 1
                self.tasklists.append(TaskList(line.strip().split('## ').pop()))

    def get_latest_task(self):
        return self.tasklists[0]

    def get_entire_text(self):
        return self.storage

    def prepend_task_from_msg(self, msg):
        latest = self.get_latest_task()
        task = Task(latest.date, False, msg)
        latest.prepend_task(task)
        return self

    def write_out(self):
        writer = Writer(self.tasklists)
        writer.write_out()
        del writer
