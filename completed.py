#!/usr/bin/env python3.10
import sys

import persistence
from util import runtime


def usage():
    print('Usage: completed <task name>', file=sys.stderr)


def main():
    mgr = persistence.Persistence()
    mgr.get_latest_task().complete_task(sys.argv.pop())
    mgr.write_out()
    return 0


if __name__ == '__main__':
    runtime(main)
