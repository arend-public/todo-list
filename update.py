#!/usr/bin/env python3.10
import sys

import persistence
from util import date_str, runtime, original


def main() -> int:
    reader = persistence.Persistence()
    if reader.get_latest_task().date == date_str:
        print('Already updated today!', sys.stderr)
        return 1
    rolling = reader.get_entire_text()
    tasks = reader.get_latest_task().incomplete_tasks_raw()

    with open(original, 'w') as file:
        print(f'## {date_str}', file=file)
        print(f'### Tasks', file=file)
        print(*tasks, file=file, sep='\n')
        print(file=file)
        print(*rolling, file=file, sep='')
    return 0


if __name__ == '__main__':
    runtime(main)
