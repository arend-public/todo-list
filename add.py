#!/usr/bin/env python3.10
import sys

import persistence
from util import runtime


def main():
    mgr = persistence.Persistence()
    mgr.prepend_task_from_msg(sys.argv.pop())
    mgr.write_out()


if __name__ == '__main__':
    runtime(main)
